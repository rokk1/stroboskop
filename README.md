# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://rokk1@bitbucket.org/rokk1/stroboskop.git

Naloga 6.2.3:
https://bitbucket.org/rokk1/stroboskop/commits/31b228424a6544f28fe7e079af60ae30bdc97502

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/rokk1/stroboskop/commits/317ca83209ac19ab345decbd2855bf53e5b6c8a9

Naloga 6.3.2:
https://bitbucket.org/rokk1/stroboskop/commits/6e44175339f45e0609be540acf8d668aeceb02a0

Naloga 6.3.3:
https://bitbucket.org/rokk1/stroboskop/commits/fcb25951f9dc5ff04d3fc2e0724b627a9d539326

Naloga 6.3.4:
https://bitbucket.org/rokk1/stroboskop/commits/c63bef3babf9652389b952177303b34fa89e20e8

Naloga 6.3.5:

git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/rokk1/stroboskop/commits/06beefbf9913b762516e58fdca5aa47d38fb2369

Naloga 6.4.2:
https://bitbucket.org/rokk1/stroboskop/commits/4eb689a4428fc4fcda4602f8bafe5e6754f905f1

Naloga 6.4.3:
https://bitbucket.org/rokk1/stroboskop/commits/6d09ad5420ce111fd0d7005849f6e829e3283b3e

Naloga 6.4.4:
https://bitbucket.org/rokk1/stroboskop/commits/041c75c83da3a8ceffa4d918b69456621e7cd58b